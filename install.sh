#!/bin/bash

if [ "$SHELL" != "/usr/bin/zsh" ];
then 
	echo "Please install zsh first with your package manager!"
	exit 1
fi

# Create folder for plugins
if [ ! -d "~/.zsh" ];
then
	mkdir ~/.zsh
fi

# Clone p10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh/zsh-syntax-highlighting

git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions

git clone https://github.com/zsh-users/zsh-history-substring-search.git ~/.zsh/zsh-history-substring-search

cp ./zshrc ~/.zshrc
source ~/.zshrc

